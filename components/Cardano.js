import { useEffect, useState } from "react";
import { connectBeacon, hexEncode } from "./utils";

export default function Cardano({
  wallet,
  setAccount,
  account,
  toggleRefresh,
}) {
  const [isInstalled, setInstalled] = useState(false);
  const [isEnabled, setEnabled] = useState(false);
  const [address, setAddress] = useState(undefined);
  const [baseAddress, setBaseAddress] = useState(undefined);
  const [status, setResponseStatus] = useState(undefined);

  // if cardano object is available (not undefined) then nami is installed,
  // check if user is connected by isEnabled method
  useEffect(() => {
    async function cardanoStatus() {
      if (typeof cardano !== "undefined") {
        setInstalled(true);
        const isEnabled = await cardano.isEnabled();
        isEnabled ? setEnabled(true) : setEnabled(false);
      } else {
        setInstalled(false);
      }
    }
    cardanoStatus();
  }, []);

  // this will be called when user clicks on Pair or Accept button
  // shows response for 3 seconds (error or success)
  useEffect(() => {
    if (typeof status !== undefined) {
      const timeout = setTimeout(() => {
        setResponseStatus(undefined);
      }, 3000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [status]);

  // if user is connected get his address (not addr1 form)
  useEffect(() => {
    async function getCardanoAddress() {
      if (isEnabled) {
        const baseAddress = await cardano.getChangeAddress();
        setBaseAddress(baseAddress);
      }
    }
    getCardanoAddress();
  }, [isEnabled]);

  // use wasm, cardano serialization lib to transform baseaddress we get
  // from `getChangeAddress()` method to addr1 format
  useEffect(() => {
    if (typeof window !== "undefined") {
      import(
        "@emurgo/cardano-serialization-lib-browser/cardano_serialization_lib_bg"
      ).then(async (emurgo) => {
        if (typeof baseAddress !== "undefined") {
          const address = emurgo.Address.from_bytes(
            Buffer.from(baseAddress, "hex")
          ).to_bech32();
          setAddress(address);
        }
      });
    }
  }, [baseAddress]);

  const connectWallet = () => {
    connectBeacon(wallet, setAccount);
  };

  const beaconStatus = () => {
    if (typeof account !== "undefined") {
      return <div className="my-2 italic truncate">{account.address}</div>;
    } else {
      return (
        <button className="btn btn-pink my-2" onClick={connectWallet}>
          Connect Tezos Wallet
        </button>
      );
    }
  };

  // used in afterInstall function, enables nami wallet
  // pop up extension will show up, give password
  const connectCardano = async () => {
    try {
      const response = await cardano.enable();
      setEnabled(response);
    } catch (e) {
      console.error(e);
    }
  };

  // if nami is installed, show connect button or address if
  // user is connected already
  const afterInstall = () => {
    if (isEnabled) {
      return <div className="pb-2 italic truncate">{address}</div>;
    } else {
      return (
        <button className="btn btn-pink" onClick={connectCardano}>
          Connect Nami Wallet
        </button>
      );
    }
  };

  // sign connected temple wallet tezos address with nami wallet
  // hex encode address then sign
  // https://github.com/Berry-Pool/nami-wallet#cardanosigndataaddress-payload
  const signTezosAddressWithNami = async () => {
    try {
      const msg = hexEncode(account.address);
      const signature = await cardano.signData(baseAddress, msg);
      return signature;
    } catch (e) {
      console.error(e);
    }
  };

  // post signature to backend
  const storePairedAddress = async (signature) => {
    let data = {
      signature: signature,
    };
    try {
      let response = await fetch("http://localhost:8080/cardano", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      setResponseStatus(response.ok);
    } catch (e) {
      setResponseStatus(false);
      console.error(e);
    }
  };

  // combine sign and store functions into one async function
  // which is called onClick
  const signAndStoreAddress = async () => {
    const signature = await signTezosAddressWithNami();
    if (typeof signature !== "undefined") {
      await storePairedAddress(signature);
      toggleRefresh((prevState) => !prevState);
    }
  };

  // disable button if tezos account is not connected
  // and if nami wallet is not connected
  const isSignable = () => {
    return account && isEnabled ? true : false;
  };

  // shows response status after Pair button is clicked
  const responseStatus = () => {
    if (typeof status !== "undefined") {
      return status ? (
        <div className="text-green-500 mt-2">Successfully paired.</div>
      ) : (
        <div className="text-red-500 mt-2">Error.</div>
      );
    } else return null;
  };

  return (
    <div className="border-pink border p-4 rounded-md m-1">
      <h1 className="font-bold">Cardano</h1>
      <p className="text-xs mb-4">Pair Cardano Address</p>
      <div>Tezos address: </div>
      <div>{beaconStatus()}</div>
      <div>Cardano address: </div>
      <div className="mb-4 mt-2">
        {isInstalled ? (
          afterInstall()
        ) : (
          <div className="pb-2 px-2">
            <a
              target="_blank"
              rel="noreferrer"
              href="https://namiwallet.io/"
              className="link"
            >
              Install Nami Wallet
            </a>
          </div>
        )}
      </div>
      <button
        className="btn btn-tr w-20 disabled:opacity-50 disabled:hover:bg-gray-darkest disabled:border-pink"
        disabled={!isSignable()}
        onClick={signAndStoreAddress}
      >
        Pair
      </button>
      {responseStatus()}
    </div>
  );
}
