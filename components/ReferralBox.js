import { useEffect, useState } from "react";
import { useRouter } from "next/dist/client/router";
import { validateAddress } from "@taquito/utils";
import {
  getContributedTier,
  sumPublicAndPresale,
  SALE_ADDRESS,
  PRESALE_ADDRESS,
  fetchAmbassador,
} from "./utils";

export default function ReferralBox({
  wallet,
  account,
  contributor,
  toggleRefresh,
  currentReferrer,
  currentReferrerPresale,
}) {
  const [status, setResponseStatus] = useState(undefined);
  const [requestedReferrer, setRequestedReferrer] = useState(undefined);
  const [requestedReferrerPresale, setRequestedReferrerPresale] =
    useState(undefined);
  const router = useRouter();
  const { address } = router.query;

  // this will be called when user clicks on Pair or Accept button
  // shows response for 3 seconds (error or success)
  useEffect(() => {
    if (typeof status !== undefined) {
      const timeout = setTimeout(() => {
        setResponseStatus(undefined);
      }, 3000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [status]);

  // get all txs for specified account (from sale and presale address)
  // useEffect checks if address in url is the same as in DB stored referrer address
  // if so pull just once, if not pull data for both addresses
  useEffect(() => {
    const fetchAll = async () => {
      if (typeof contributor !== "undefined") {
        if (contributor.referrer_tezos_address === address) {
          setRequestedReferrer(currentReferrer);
          setRequestedReferrerPresale(currentReferrerPresale);
        } else {
          setRequestedReferrer(await fetchAmbassador(address, SALE_ADDRESS));
          setRequestedReferrerPresale(
            await fetchAmbassador(address, PRESALE_ADDRESS)
          );
        }
      }
    };

    fetchAll();
  }, [account, address, contributor]);

  const signAmbassadorAddress = async () => {
    const input = ["Tezos Signed Message:", address].join(" ");
    // https://docs.walletbeacon.io/guides/sign-payload
    // micheline example actually printed readable text in temple
    // 050100000042 it works with this prefix
    const magicPrefix = "050100000042";
    const bytes = magicPrefix + Buffer.from(input, "utf8").toString("hex");
    const payload = {
      signingType: "micheline",
      payload: bytes,
    };

    try {
      const raw = await wallet.requestSignPayload(payload);
      return {
        address: account.address,
        signature: raw.signature,
        msg: bytes,
        pk: account.publicKey,
      };
    } catch (e) {
      console.error(e);
    }
  };

  const pairReferrerWithAddress = async (body) => {
    try {
      const response = await fetch("http://localhost:8080/set_referrer", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      setResponseStatus(response.ok);
    } catch (e) {
      setResponseStatus(false);
      console.log(e);
    }
  };

  // basically the same thing is in cardano and ethereum file,
  // sign msg post things on the server a this just combines it
  // so I can put it in onClick event
  // pull contributor afterwards (toggle refresh)
  const signAndStoreAddress = async () => {
    const body = await signAmbassadorAddress();
    if (typeof body?.signature !== "undefined") {
      await pairReferrerWithAddress(body);
      toggleRefresh((prevState) => !prevState);
    }
  };

  const responseStatus = () => {
    if (typeof status !== "undefined") {
      return status ? (
        <div className="text-green-500 mt-2 px-2">Successfully paired.</div>
      ) : (
        <div className="text-red-500 mt-2 px-2">Error.</div>
      );
    } else return null;
  };

  // if tezos temple wallet is not connected or if
  // address in url query string is not valid tezos address
  const disableButton = () => {
    return (typeof account === "undefined" ? true : false) ||
      (validateAddress(address) !== 3 ? true : false) ||
      account?.address === address
      ? true
      : false;
  };

  return (
    <div className="max-w-screen-lg mx-auto">
      <div className="border-pink border p-4 rounded-md mt-4 m-1">
        <div className="flex mt-4 font-bold my-2 p-2">
          {`SEXP ambassador ${address} is offering you a
          bonus.`}
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 my-4">
          <div className="p-2">
            <div>
              {`Ambassador public sale Hype bonus: ${getContributedTier(
                requestedReferrer
              )}%`}
            </div>
            <div>
              {`Ambassador presale Hype bonus: ${getContributedTier(
                requestedReferrerPresale
              )}%`}
            </div>
            <div className="text-green-500">{`Both of you receive ${sumPublicAndPresale(
              requestedReferrer,
              requestedReferrerPresale
            )}% bonus SEXP`}</div>
          </div>

          <div className="p-2">
            <div>
              {`Current ambassador public sale Hype bonus: ${getContributedTier(
                currentReferrer
              )}%`}
            </div>
            <div>
              {`Current ambassador presale Hype bonus: ${getContributedTier(
                currentReferrerPresale
              )}%`}
            </div>

            <div className="text-pink-light">{`Current bonus ${sumPublicAndPresale(
              currentReferrer,
              currentReferrerPresale
            )}% bonus SEXP`}</div>
          </div>
        </div>

        <div className="p-2">
          <button
            disabled={disableButton()}
            className="btn-tr w-36 disabled:opacity-50 disabled:hover:bg-gray-darkest disabled:border-pink"
            onClick={signAndStoreAddress}
          >
            Accept
          </button>
        </div>
        {responseStatus()}
      </div>
    </div>
  );
}
