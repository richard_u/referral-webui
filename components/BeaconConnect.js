import Link from "next/link";
import { connectBeacon, disconnectBeacon } from "./utils";

export default function BeaconConnect({ wallet, setAccount, account }) {
  const connectWallet = () => {
    connectBeacon(wallet, setAccount);
  };

  const disconnectWallet = () => {
    disconnectBeacon(wallet, setAccount);
  };

  // shows connect tezos or connected with address
  // in "navigation" bar at the top
  const connectStatus = () => {
    if (typeof account !== "undefined") {
      return (
        <div
          className="w-60 text-center text-xs font-bold link px-2 py-1 border-pink border rounded-md my-2 m-1"
          onClick={disconnectWallet}
        >
          <div className="flex p-1">
            <div className="inline-block mr-1 mt-1.5 h-1.5 w-3.5 bg-green-500 rounded-full" />
            <div className="mr-1 text-sm">Connected</div>
            <div className="mr-1 text-sm">|</div>
            <div className="truncate text-sm font-normal">
              {account.address}
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <button
          className="btn btn-tr text-xs mx-2 my-2"
          onClick={connectWallet}
        >
          Connect Tezos Wallet
        </button>
      );
    }
  };

  return (
    <div className="flex justify-between">
      <div className="text-lg italic hover:text-pink-light px-2 py-1 my-2 m-1">
        <Link href="/">sexp</Link>
      </div>
      {connectStatus()}
    </div>
  );
}
