export default function ReferralLink({ account }) {
  const clipboard = `http://localhost:3000?address=${account?.address}`;

  const copyToClipboard = () => {
    navigator.clipboard.writeText(clipboard);
  };

  return (
    <div className="border-pink border p-4 rounded-md m-1">
      <div className="text-center font-bold">Referral Link</div>
      <div className="flex gap-2 mt-4">
        <input
          disabled={true}
          className="text-black w-full p-2 rounded text-sm"
          value={clipboard}
        ></input>
        <button className="btn btn-tr" onClick={copyToClipboard}>
          Copy
        </button>
      </div>
    </div>
  );
}
