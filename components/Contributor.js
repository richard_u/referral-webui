import { addEthereumPrefix } from "./utils";

export default function Contributor({ contributor, bonus }) {
  const printCardanoAddresses = () => {
    return contributor.contributor_cardano_addresses.map((item, index) => {
      return (
        <div className="italic truncate text-center my-1" key={index}>
          {item}
        </div>
      );
    });
  };

  const printEthereumAddresses = () => {
    return contributor.contributor_evm_addresses.map((item, index) => {
      return (
        <div className="italic truncate text-center my-1" key={index}>
          {addEthereumPrefix(item)}
        </div>
      );
    });
  };

  const printContributorBox = () => {
    if (typeof contributor !== "undefined") {
      if (contributor.contributor_tezos_address) {
        return (
          <div className="my-4">
            <div className="border-pink border p-4 rounded-md m-1">
              <div className="flex justify-center my-2">
                <div className="text-center font-bold">Contributor</div>
              </div>
              <div className="flex justify-between">
                Contributor:
                <div className="italic truncate">
                  {contributor.contributor_tezos_address}
                </div>
              </div>
              <div className="flex justify-between">
                Referrer (current bonus {bonus}%):
                <div className="italic truncate">
                  {contributor.referrer_tezos_address}
                </div>
              </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 mt-4">
              <div className="border-pink border p-4 rounded-md m-1">
                <div className="text-center font-bold">
                  Contributor Cardano addresses
                </div>
                {printCardanoAddresses()}
              </div>
              <div className="border-pink border p-4 rounded-md m-1">
                <div className="text-center font-bold">
                  Contributor EVM addresses
                </div>
                {printEthereumAddresses()}
              </div>
            </div>
          </div>
        );
      } else {
        return null;
      }
    } else {
      return null;
    }
  };

  return printContributorBox();
}
