const hexEncode = (str) => {
  var result = "";
  for (var i = 0; i < str.length; i++) {
    result += str.charCodeAt(i).toString(16);
  }
  return result;
};

const connectBeacon = async (wallet, setAccount) => {
  try {
    const account = await wallet.requestPermissions();
    setAccount(account);
  } catch (e) {
    console.error(e);
  }
};

const disconnectBeacon = async (wallet, setAccount) => {
  await wallet.clearActiveAccount();
  setAccount(undefined);
};

const sliceEthereumPrefix = (str) => {
  return str.slice(2);
};

const addEthereumPrefix = (str) => {
  return "0x" + str;
};

const SALE_ADDRESS = "tz1hwmXHNVC5RdFu22uT7fPR638yftUn7YrD";
const PRESALE_ADDRESS = "tz1VpowGiMcNdMUtCo1uRda26fCsnTJTpwFh";
const makeUrl = (address, saleAddr) => {
  return `https://api.mainnet.tzkt.io/v1/accounts/${saleAddr}/operations?type=transaction&limit=40&sort=1&quote=usd&sender=${address}`;
};

const fetchAmbassador = async (ambassadorAddress, saleAddress) => {
  try {
    const response = await fetch(makeUrl(ambassadorAddress, saleAddress));
    if (response.ok) {
      return await response.json();
    } else {
      return undefined;
    }
  } catch (e) {
    return undefined;
  }
};

// take result from tzkt.io get txs call, iterate over array of results
// take amount and multiply that by usd price at that time, sum result
const getContributedTezos = (ambassador) => {
  if (typeof ambassador !== "undefined") {
    const add = (acc, x) => {
      return acc + x;
    };
    const amounts = ambassador.map((item) => {
      return item.amount * item.quote.usd;
    });
    const sum = amounts.reduce(add, 0);
    return sum;
  }
};

const mapAmbassadorTier = (value) => {
  if (value > 100 && value < 2000) {
    return 1;
  } else if (value > 2000 && value < 10000) {
    return 2.5;
  } else if (value > 10000) {
    return 5;
  } else {
    return 0;
  }
};

// used in jsx = shorter, so i dont have to chain function calls
const getContributedTier = (ambassador) => {
  const sum = getContributedTezos(ambassador);
  const precision = 1000000;
  const tezosUSD = sum / precision;
  return mapAmbassadorTier(Math.round(tezosUSD));
};

const getContributedUSDValue = (ambassador) => {
  const sum = getContributedTezos(ambassador);
  const precision = 1000000;
  const tezosUSD = sum / precision;
  return tezosUSD;
};

const sumPublicAndPresale = (publ, presale) => {
  const pub = getContributedUSDValue(publ);
  const pre = getContributedUSDValue(presale);
  return mapAmbassadorTier(pub + pre);
};

export {
  connectBeacon,
  disconnectBeacon,
  hexEncode,
  sliceEthereumPrefix,
  addEthereumPrefix,
  sumPublicAndPresale,
  getContributedTier,
  makeUrl,
  SALE_ADDRESS,
  PRESALE_ADDRESS,
  fetchAmbassador,
};
