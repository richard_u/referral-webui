export default function Content() {
  return (
    <div className="text-center mt-4 max-w-xl mx-auto px-2 my-10">
      <h1 className="font-bold my-2">SEXP Public Sale</h1>
      <p>
        Welcome to the SEXP public sale. Lorem ipsum dolor sit amet, consetetur
        sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
        dolore magna aliquyam.
      </p>
      <p>
        Contributions are accepted in XTZ on Tezos, ADA on Cardano, USDT on
        Ethereum, BSC & Avalanche.
      </p>
    </div>
  );
}
