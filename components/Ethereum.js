import { useEffect, useState } from "react";
import Onboarding from "@metamask/onboarding";
import { connectBeacon, hexEncode, sliceEthereumPrefix } from "./utils";

export default function Ethereum({
  wallet,
  setAccount,
  account,
  toggleRefresh,
}) {
  const [isInstalled, setInstalled] = useState(false);
  const [isConnected, setConnected] = useState(false);
  const [address, setAddress] = useState(undefined);
  const [status, setResponseStatus] = useState(undefined);

  useEffect(() => {
    const { ethereum } = window;
    setInstalled(Boolean(ethereum && ethereum.isMetaMask));
  }, []);

  useEffect(() => {
    if (typeof status !== undefined) {
      const timeout = setTimeout(() => {
        setResponseStatus(undefined);
      }, 3000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [status]);

  useEffect(() => {
    async function isAccountConnected() {
      if (typeof ethereum !== "undefined") {
        const account = await ethereum.request({ method: "eth_accounts" });
        if (account[0]) {
          setAddress(account[0]);
          setConnected(true);
        } else {
          setConnected(false);
        }
      }
    }
    isAccountConnected();
  }, [isConnected]);

  const connectWallet = () => {
    connectBeacon(wallet, setAccount);
  };

  const beaconStatus = () => {
    if (typeof account !== "undefined") {
      return <div className="my-2 italic truncate">{account.address}</div>;
    } else {
      return (
        <button className="btn btn-pink my-2" onClick={connectWallet}>
          Connect Tezos Wallet
        </button>
      );
    }
  };

  const connectToMetaMask = async () => {
    try {
      await ethereum.request({ method: "eth_requestAccounts" });
      setConnected(true);
    } catch (e) {
      console.error(e);
    }
  };

  const installMetaMask = () => {
    try {
      const onboarding = new Onboarding();
      onboarding.startOnboarding();
    } catch (e) {
      console.error(e);
    }
  };

  const metaMaskClientCheck = () => {
    if (!isInstalled) {
      return (
        <div className="link" onClick={installMetaMask}>
          Click here to install MetaMask.
        </div>
      );
    } else if (!isConnected) {
      return (
        <button className="btn btn-pink" onClick={connectToMetaMask}>
          Connect MetaMask
        </button>
      );
    } else {
      return <div className="my-2 pb-2 italic truncate">{address}</div>;
    }
  };

  const signAddressWithMetaMask = async () => {
    try {
      let msg = `0x${hexEncode(account.address)}`;
      const signature = await ethereum.request({
        method: "personal_sign",
        params: [address, msg],
      });
      return { signature, msg };
    } catch (e) {
      console.error(e);
    }
  };

  const storePairedAddress = async (signature, msg) => {
    let data = {
      address: sliceEthereumPrefix(address),
      signature: sliceEthereumPrefix(signature),
      msg: sliceEthereumPrefix(msg),
    };
    try {
      let response = await fetch("http://localhost:8080/ethereum", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      setResponseStatus(response.ok);
    } catch (e) {
      setResponseStatus(false);
      console.error(e);
    }
  };

  const signAndStoreAddress = async () => {
    const response = await signAddressWithMetaMask();
    if (typeof response?.signature !== "undefined") {
      await storePairedAddress(response.signature, response.msg);
      toggleRefresh((prevState) => !prevState);
    }
  };

  const isSignable = () => {
    return account && address ? true : false;
  };

  const responseStatus = () => {
    if (typeof status !== "undefined") {
      return status ? (
        <div className="text-green-500 mt-2">Successfully paired.</div>
      ) : (
        <div className="text-red-500 mt-2">Error.</div>
      );
    } else return null;
  };

  return (
    <div className="border-pink border p-4 rounded-md m-1">
      <div className="font-bold">Ethereum</div>
      <p className="text-xs mb-4">Pair Ethereum Address</p>
      <div>Tezos address: </div>
      <div>{beaconStatus()}</div>
      <div>Ethereum address: </div>
      <div className="mb-4 mt-2">{metaMaskClientCheck()}</div>
      <button
        className="btn btn-tr w-20 disabled:opacity-50 disabled:hover:bg-gray-darkest disabled:border-pink"
        disabled={!isSignable()}
        onClick={signAndStoreAddress}
      >
        Pair
      </button>
      {responseStatus()}
    </div>
  );
}
