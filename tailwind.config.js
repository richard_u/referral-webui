const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      blue: {
        light: "#85d7ff",
        DEFAULT: "#1fb6ff",
        dark: "#009eeb",
      },
      pink: {
        light: "#d693ed",
        DEFAULT: "#e67bf1",
        dark: "#953fca",
      },
      gray: {
        darkest: "#14082a",
        dark: "#3c4858",
        DEFAULT: "#c0ccda",
        light: "#e0e6ed",
        lightest: "#f9fafc",
      },
      green: colors.green,
      white: colors.white,
      red: colors.red,
      black: colors.black,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
