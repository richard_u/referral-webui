import { useEffect, useState } from "react";
import { useRouter } from "next/dist/client/router";
import Head from "next/head";
import {
  fetchAmbassador,
  PRESALE_ADDRESS,
  SALE_ADDRESS,
  sumPublicAndPresale,
} from "../components/utils";
import BeaconConnect from "../components/BeaconConnect";
import Cardano from "../components/Cardano";
import Content from "../components/Content";
import Contributor from "../components/Contributor";
import Ethereum from "../components/Ethereum";
import ReferralBox from "../components/ReferralBox";
import ReferralLink from "../components/ReferralLink";

export default function Home({ wallet, setAccount, account }) {
  const [contributor, setContributor] = useState(undefined);
  const [refresh, toggleRefresh] = useState(false);
  const [currentReferrer, setCurrentReferrer] = useState(undefined);
  const [currentReferrerPresale, setCurrentReferrerPresale] =
    useState(undefined);

  // check if address is in query string, show box only if address is
  // not undefined
  const router = useRouter();
  const { address } = router.query;

  // pull contributor from DB if user is connected
  // effect is called on first load, on account change
  // and when refresh is toggled (after Accept or Pair button click)
  // to pull new data from DB
  useEffect(() => {
    const getContributor = async () => {
      if (typeof account !== "undefined") {
        try {
          const response = await fetch(
            `http://localhost:8080/contributor/${account.address}`
          );
          setContributor(await response.json());
        } catch (e) {
          console.error(e);
        }
      } else {
        setContributor(undefined);
      }
    };

    getContributor();
  }, [account, refresh]);

  // check if connected user has referrer, if so pull
  // data from tzkt and set it as current referrer
  // show bonus in contribution box
  // send state to child component ReferralBox where
  // we compare the bonuses with requested referrer
  // this is called when contributor is changed or
  // on first render
  useEffect(() => {
    const fetchCurrentReferrer = async () => {
      if (typeof contributor !== undefined) {
        if (contributor?.referrer_tezos_address) {
          const sale = await fetchAmbassador(
            contributor.referrer_tezos_address,
            SALE_ADDRESS
          );
          const presale = await fetchAmbassador(
            contributor.referrer_tezos_address,
            PRESALE_ADDRESS
          );

          setCurrentReferrer(sale);
          setCurrentReferrerPresale(presale);
        }
      }
    };
    fetchCurrentReferrer();
  }, [contributor]);

  return (
    <div className="max-w-screen-lg mx-auto">
      <Head>
        <title>SEXP Public Sale</title>
        <meta charSet="utf-8" />
      </Head>
      <BeaconConnect
        wallet={wallet}
        setAccount={setAccount}
        account={account}
      />
      <Content />
      {address ? (
        <ReferralBox
          wallet={wallet}
          account={account}
          contributor={contributor}
          toggleRefresh={toggleRefresh}
          currentReferrer={currentReferrer}
          currentReferrerPresale={currentReferrerPresale}
        />
      ) : null}
      {account ? <ReferralLink account={account} /> : null}
      <Contributor
        contributor={contributor}
        bonus={sumPublicAndPresale(currentReferrer, currentReferrerPresale)}
      />
      <div className="grid grid-cols-1 md:grid-cols-2 mt-4 text-center">
        <Cardano
          wallet={wallet}
          setAccount={setAccount}
          account={account}
          toggleRefresh={toggleRefresh}
        />
        <Ethereum
          wallet={wallet}
          setAccount={setAccount}
          account={account}
          toggleRefresh={toggleRefresh}
        />
      </div>
    </div>
  );
}
