import { useEffect, useState } from "react";

import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  const [wallet, setWallet] = useState(undefined);
  const [account, setAccount] = useState(undefined);

  useEffect(() => {
    if (typeof window !== "undefined") {
      import("@airgap/beacon-sdk").then(async (module) => {
        const wallet = new module.DAppClient({
          name: "SEXP referral",
          preferredNetwork: module.NetworkType.GRANADANET,
          colorMode: module.ColorMode.DARK,
        });
        setWallet(wallet);
        const activeAccount = await wallet.getActiveAccount();
        if (activeAccount) {
          setAccount(activeAccount);
        }
      });
    }
  }, []);

  return (
    <Component
      {...pageProps}
      wallet={wallet}
      setAccount={setAccount}
      account={account}
    />
  );
}

export default MyApp;
